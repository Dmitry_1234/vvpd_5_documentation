## Программы выполнения нагрева жидкости

[Система нагрева жидкости](https://www.google.com/imgres?imgurl=https%3A%2F%2Ftermanik.ru%2Fwp-content%2Fuploads%2F2018%2F11%2Ftermanik-bojler-50-s-emkostyu-zakazchika-.png&imgrefurl=https%3A%2F%2Ftermanik.ru%2Fproduct%2Fpodogrev-rezervuarov%2F&tbnid=SyuzEAID_W8GVM&vet=12ahUKEwi7y4DYofH7AhUwDRAIHdFRC04QMygKegQIARBg..i&docid=yaFslVIHxQgzOM&w=1800&h=1197&)

Как она работает можно посмотреть [здесь](https://shop.mosoblgaz.ru/articles/kak-rabotaet-boiler-dlya-vody/)

Данная программа реализует _хранение информации о
программах выполнения технологического процесса нагрева жидкости_
.

**Структура**: наименование, список длительности временных
интервалов, список температурных уставок, список дополнительных
параметров.

**Программа позволяет**:
* загружать информацию из файла;
* выполнять поис+к по наименованию;
* фильтровать по количеству временных интервалов;
* добавлять записи;
* удалять записи;
* сохранять данные в файле.

**Функции программы**
- [ ] print_inf
- [ ] research_inf
- [ ] filter_inf
- [ ] append_inf
- [ ] delete_inf
- [ ] save_inf
- [ ] end
- [ ] main
- [ ] start

В программе используется
`import json`

~~Формулы в программе не используются~~
$E=mc^2$
